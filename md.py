from markdown import Markdown
from markdown.extensions.toc import TocExtension


md = Markdown(extensions=[
    'extra', 
    'codehilite', 
    TocExtension(
        # permalink=True,
        baselevel=2,
    ),
])
