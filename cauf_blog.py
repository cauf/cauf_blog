from os import abort
from flask import (
    Flask,
    render_template,
    request,
    redirect,
    url_for,
)
from pony.orm.core import select
from storage import (
    db,
    Article,
    Tag,
    NormalTag,
    User,
)
from pony.flask import Pony
import time
import math


app = Flask("cauf_blog")
Pony(app)

@app.route('/')
def home():
    # return render_template('base.html')
    return redirect(url_for('article_all'))

@app.route('/article/all', methods=['get'])
def article_all():
    pagenum = 1
    if 'page' in request.args.keys():
        pagenum = int(request.args.get('page'))
    articles_count = Article.select().count()
    max_page = math.ceil(articles_count / 10)
    if (max_page < pagenum) or (pagenum < 1):
        return redirect('e404')
    else:
        articles = Article.select().sort_by(Article.public_dt)[10*(pagenum - 1):10*pagenum]
        start_page = 1 if (articles_count < 30) or (pagenum < 3) else (pagenum - 3)
        end_page = max_page if (max_page - pagenum) <= 3 else pagenum + 3
        return render_template(
            'article_all.html', 
            articles = articles, 
            start_page = start_page,
            cur_page = pagenum,
            end_page = end_page,
            max_page = max_page,
        )

@app.route('/article/new', methods=['get', 'post'])
def article_new():
    if request.method == 'POST':
        art = Article()
        art.caption = request.form.get('caption')
        art.text_md = request.form.get('text_md')
        art.preview_md = request.form.get('preview_md')
        art.compile()
        db.commit()
        return redirect(url_for('article_show', art_id = art.id))
    return render_template('article_new.html')

@app.route('/article/<int:art_id>')
def article_show(art_id):
    if art_id:
        art = Article.select(lambda a: a.id == art_id).first()
        if art:
            return render_template(
                'article_show.html',
                id = art.id,
                caption=art.caption,
                preview=art.preview_html,
                text=art.text_html,
                tags = {t.id:t.text for t in art.tags},
            )
    return redirect(url_for('e404'), code=404)

@app.route('/article/<int:art_id>/edit')
def article_edit(art_id):
    return redirect(url_for('article_show', art_id=art_id))

@app.route('/article/<int:art_id>/recompile')
def article_recompile(art_id):
    if art_id:
        art = Article.select(lambda a: a.id == art_id).first()
        if art:
            art.compile()
            db.commit()
            return redirect(url_for('article_show', art_id=art.id))
    return redirect(url_for('e404'), code=404)

@app.route('/article/<int:art_id>/delete')
def article_delete(art_id):
    if art_id:
        art = Article.select(lambda a: a.id == art_id).first()
        if art:
            art.delete()
            db.commit()
            return redirect(url_for('article_all'))
    return redirect(url_for('e404'), code=404)

@app.route('/login', methods=['get', 'post'])
def login():
    if request.method == 'POST':
        login = request.form.get('login')
        password = request.form.get('password')
        return redirect(url_for('article_all'))
    else:
        return render_template('login.html')

@app.route('/reg', methods=['get', 'post'])
def reg():
    msg = ''
    if request.method == 'POST':
        is_reg = True
        login = request.form.get('login')
        pswd1 = request.form.get('password1')
        pswd2 = request.form.get('password2')
        email = request.form.get('email')
        if pswd1 != pswd2:
            msg = 'Введенные пароли не совпадают'
            is_reg = False
        if len(pswd1) < 6:
            msg = 'Введенный пароль менее 6 символов'
            is_reg = False
        if len(User.select(lambda u: u.username == login)) > 0:
            msg = 'Введенный логин уже занят'
            is_reg = False
        if is_reg:
            usr = User.reg(
                login=login,
                password=pswd1,
                email=email,
            )
            db.commit()
    return render_template('reg.html', msg=msg)

@app.route('/tag/<int:tag_id>', methods=['get'])
def tag_show(tag_id):
    pagenum = 1
    tag = Tag[tag_id]
    if 'page' in request.args.keys():
        pagenum = int(request.args.get('page'))
    articles_all = Article.select(lambda a: tag in a.tags).sort_by(Article.public_dt)
    articles_count = articles_all.count()
    max_page = math.ceil(articles_count / 10)
    if (max_page < pagenum) or (pagenum < 1):
        abort(404)
    else:
        articles = Article.select(lambda a: tag in a.tags).sort_by(Article.public_dt)[10*(pagenum - 1):10*pagenum]
        start_page = 1 if (articles_count < 30) or (pagenum < 3) else (pagenum - 3)
        end_page = max_page if (max_page - pagenum) <= 3 else pagenum + 3
        return render_template(
            'tag_show.html', 
            tag_id = tag_id,
            tag = tag.text,
            articles = articles, 
            articles_count = articles_count,
            start_page = start_page,
            cur_page = pagenum,
            end_page = end_page,
            max_page = max_page,
        )

@app.route('/error/404')
def e404():
    return render_template('404.html')
