caption: Проверка компиляции разметки Markdown
preview: |
  Проверка компиляции разметки Markdown из тестового файла с применением 
  дополнительной информации из `yaml`-разметки.
tags:
  - test
  - markdown
  - blog

*****

# Тест разметки Markdown



## Заголовки

# Заголовок H1
## Заголовок H2
### Заголовок H3
#### Заголовок H4
##### Заголовок H5
###### Заголовок H6



## Форматирование текста

*Курсив* и _Курсив_

**Жирный текст** и __Жирный текст__

***Жирный и курсивный текст***

~~Зачёркнутый текст.~~



## Цитаты

> Очень глубокомысленная цитата. Пожалуйста, прочтите её внимательно.
>
> Оскар Уайльд



## Маркированный список

- Первый пункт.
- Второй пункт.
- Третий пункт.



## Нумерованный список

1. Первый пункт.
2. Второй пункт.
3. Третий пункт.



## Многоуровневый список

1. Первый пункт.
    - Первый подпункт.
    - Второй подпункт.
    - Третий подпункт.
2. Второй пункт.
    - Первый подпункт.
    - Второй подпункт.
    - Третий подпункт.



## Горизонтальные линии

***

---

___



## Ссылки и изображения

[Ссылка на цветовую схему сайта](https://colorscheme.ru/#3Lc2Nw0w0w0w0)

![](https://lor-sh.ams3.cdn.digitaloceanspaces.com/lor-sh/cache/media_attachments/files/105/460/822/022/124/283/original/5eb01b247e5b0ef1.jpeg)



## Таблицы

| dolores sit necessitatibus | a voluptates et | sint fuga quaerat |
|----|----|----|
| quia et in | Velit id qui minus ipsam voluptatem cupiditate deserunt nihil explicabo. | Rerum porro excepturi dolorem provident dolorem voluptas eius. |
| ut ea est | Et eius aut perspiciatis dicta id. | Expedita dicta eum autem non voluptatem qui sit omnis. |
| et quia id | Nisi praesentium assumenda officiis inventore tenetur ea iusto sed. | Excepturi voluptas repellat suscipit tenetur provident ut. |
| veniam nisi consectetur | Dolores est natus vero. | Expedita praesentium velit provident vitae. |



## Чек-лист

- [ ] Невыполненная задача
- [ ] Невыполненная задача
- [X] Выполненная задача



## Код

```python
@app.route('/')
def home():
    # return render_template('base.html')
    return redirect(url_for('article_all'))
```

Инлайновый вариант `кода`
