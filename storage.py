from datetime import datetime
from pony.orm import *
from md import md
from hashlib import sha3_224, sha512
from pathlib import Path
from yaml import load, FullLoader


SLUG_CHARS = [chr(i) for i in range(97,123)] + \
    [chr(i) for i in range(48,58)] + \
    [chr(i) for i in range(1072,1104)]


db = Database()


class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    username = Required(str)
    pswd_hash = Required(str)
    email = Required(str)
    reg_dt = Optional(datetime)
    is_active = Optional(bool, default=False)
    last_login = Optional(datetime)
    role = Optional('Role')
    is_admin = Required(bool, default=False)
    articles = Set('Article')
    sessions = Set('Session')

    @db_session
    def reg(login:str, password:str, email:str):
        salt_pswd_hash = get_pswd_hash(login, password)
        usr = User(
            username = login,
            pswd_hash = salt_pswd_hash,
            email = email,
            is_admin = False,
        )
        db.commit()
        return usr

    @db_session
    def auth(login:str, password:str):
        usrs = User.select(lambda u: u.username == login)
        if len(usrs) == 1:
            usr = usrs.first()
            salt_pswd_hash = get_pswd_hash(login, password)
            if usr.pswd_hash == salt_pswd_hash:
                return usr
        # else return AnonUser
        return User.select(lambda u: u.id == 1).first()


class NormalTag(db.Entity):
    id = PrimaryKey(int, auto=True)
    text = Optional(str)
    tags = Set('Tag')


class Tag(db.Entity):
    id = PrimaryKey(int, auto=True)
    text = Optional(str)
    normal_tag = Required(NormalTag)
    articles = Set('Article')

    @db_session
    def create(text:str):
        ntext = normalize(text)
        tg_in_db = Tag.select(lambda t: t.normal_tag.text == ntext)
        if tg_in_db.count() > 0:
            tg_with_eq_text = tg_in_db.filter(text=text)
            if tg_with_eq_text.count() > 0:
                return tg_with_eq_text.first()
        ntg_in_db = NormalTag.select(lambda n: n.text == ntext)
        if ntg_in_db.count() > 0:
            ntg = ntg_in_db.first()
        else:
            ntg = NormalTag(
                text = ntext,
            )
        tg = Tag(
            text = text,
            normal_tag = ntg,
        )
        return tg


class Article(db.Entity):
    id = PrimaryKey(int, auto=True)
    caption = Optional(str)
    preview_md = Optional(str)
    preview_html = Optional(str)
    text_md = Optional(LongStr)
    text_html = Optional(LongStr)
    edit_dt = Optional(datetime)
    compile_dt = Optional(datetime)
    tags = Set('Tag')
    shows_count = Optional(int, size=64, default=0)
    author = Optional('User')
    create_dt = Required(datetime, default=lambda: datetime.now())
    public_dt = Optional(datetime, default=lambda: datetime.now())
    
    @db_session
    def compile(self):
        self.text_html = md.convert(self.text_md)
        self.preview_html = md.convert(self.preview_md)
        self.compile_dt = datetime.now()
    
    @db_session
    def read_from_file(filepath:str):
        fp = Path(filepath)
        if fp.exists():
            with open(fp, mode='r', encoding='utf-8') as mdf:
                raw_text = mdf.read()
            yaml_raw, md = raw_text.split('\n\n*****\n\n')
            yaml_data = load(yaml_raw, Loader=FullLoader)
            art = Article(
                caption=yaml_data['caption'],
                preview_md=yaml_data['preview'],
            )
            if 'public_dt' in yaml_data.keys():
                art.public_dt = datetime.fromtimestamp(yaml_data['public_dt'])
            if 'tags' in yaml_data.keys():
                tags = []
                for tg in yaml_data['tags']:
                    ntg_text = normalize(tg)
                    tg_in_db = select(t for t in Tag if t.normal_tag.text == ntg_text)
                    if tg_in_db.count() > 0:
                        tags.append(tg_in_db.first())
                    else:
                        ntg = NormalTag(text=ntg_text)
                        db.commit()
                        tags.append(Tag(
                            text=tg,
                            normal_tag=ntg,
                        ))
                art.tags = tags
            art.text_md = md
            db.commit()
            art.compile()
            print(f'Статья "{art.caption}" внесена в базу данных и будет опубликована {art.public_dt.isoformat()}')
        else:
            print(f'По указанному пути файл не обнаружен')


class Role(db.Entity):
    id = PrimaryKey(int, auto=True)
    caption = Optional(str)
    permissions = Set('Permission')
    users = Set(User)


class Permission(db.Entity):
    id = PrimaryKey(int, auto=True)
    caption = Optional(str)
    roles = Set(Role)


class Session(db.Entity):
    id = PrimaryKey(int, auto=True)
    code = Optional(str)
    start_dt = Optional(datetime)
    csrf = Optional(str)
    csrf_dt = Optional(datetime)
    user = Required(User)


@db_session
def recompile_all():
    arts = list(Article.select())
    for a in arts:
        a.compile()


def get_pswd_hash(login:str, password:str) -> str:
    pswd_hash = sha512(password, usedforsecurity=True).hexdigest()
    usrnm_hash = sha512(login, usedforsecurity=True).hexdigest()
    return f'{pswd_hash}{usrnm_hash[:10]}'


def normalize(text:str) -> str:
    ltext = text.lower()
    res = ''.join([c if c in SLUG_CHARS else '' for c in ltext])
    return res


db.bind(provider='sqlite', filename='test.db', create_db=True)
db.generate_mapping(create_tables=True)

if __name__ == "__main__":
    for i in range(100):
        Article.read_from_file('test_md.md')
